# Active matter

Active matter encompasses all those systems composed by self-propelling constituents. In particular this class of systems include biological and synthetic micro and nano-swimmers. All these microparticles are capable of taking up energy from their environment and converting it into directed motion. Because of this constant flow of energy, their behavior can be explained and understood only within the framework of nonequilibrium physics. In the biological realm, many cells perform directed motion, for example, as a way to search for nutrients. Moreover  artificial active particles have been also developed exploiting different propulsion mechanisms. These man-made micro and nano-machines hold a great potential as autonomous agents for many applications [1].

## Synthetic and biological swimmers

Understanding the details of the propulsion mechanism of active microparticles is of crucial importance for predicting their collective behavior and for practical applications. Typically synthetic active particles are propelled by chemical reactions as in the case of Janus particles where an asymmetric metallic coating catalyzes the dissociation of hydrogen peroxide thus producing a concentration gradient, which in turn triggers the motion of the particle.
It as been shown that thee particles can self-assemble into ordered rotating micro-structures [2].
It has also been shown that very efficient active micromotors can be designed and produced by exploiting alternative propulsion mechanism such as the Marangoni forces present at the interfaces [3].
More generally it is very interesting to understand of the propulsion mechanism of active microparticles is altered by the presence of interfaces and/or obstacles. From the biological point of view it was shown by using advanced 3D holographic microscopy techniques that E. Coli bacteria accumulate over bounding walls and tend to swim in proximity of the interfaces for long times because of a cell-wall hydrodynamic interaction [4] (Fig.1). 

<img align="center" width=75% src="3dwall.jpeg">

**Fig.1:** *Working principle of three-axis holographic microscopy. (a) The sample is illuminated by three partially coherent beams. (b) Resulting hologram arising from the interference between scattered and unscattered light. (c) A volumetric image of an E. Coli cell . (d) Sequence of volumetric reconstructions of swimming cells during a wall-entrapment event.*


## Statistical mechanics of active matter



The stochastic nature of the reorientations of active particles, both of synthetic and biological origin, makes essential to use the tools of statistical mechanics to characterize and understand their behavior on large length and/or time-scales. For example motile cells often explore natural environments characterized by a high degree of structural complexity. Moreover cell motility is also intrinsically noisy due to spontaneous random reorientations and speed fluctuations. This interplay of internal and external noise sources gives rise to a complex dynamical behavior that can be strongly sensitive to details and hard to model quantitatively. However some statistical regularities are often found when studying the dynamics of active particles even in disordered environments. For example it has been shown that the mean residence time of swimming bacteria inside artificial complex microstructures is quantitatively predicted by a generic invariance property of random walks [5] (Fig.2).

<img align="center" width=50% src="invariance.jpeg">

**Fig.2:** *Optical microscopy images of three sample microstructures with number of obstacles 7, 55, and 119. The scalebar is 20 microns. The colored lines are sample trajectories of swimming bacteria inside the micro-structures. As the number of obstacles increases, trajectories become more irregular due to scattering by obstacles.*


## Light controllable active matter

Many motile microorganisms react to environmental light cues with a variety of motility responses guiding cells towards better conditions for survival and growth. The use of light-shaping techniques could help to elucidate the mechanisms of photo-movements while, 
at the same time, providing an efficient strategy to achieve spatial and temporal control of cell
concentration. *E. Coli* cells can be genetically modified to swim smoothly with a light controllable speed, can be arranged into complex and reconfigurable density patterns using a digital light projector. It has been shown that a homogeneous sea of freely swimming bacteria can be made to morph between complex shapes by using dynamic projected light patterns [6] (Fig.3). 

<img align="center" width=50% src="monalisa.jpeg">

**Fig.3:** *A structured light pattern turns an initially homogeneous suspension of photokinetic E. Coli bacteria into a millimeter-sized reproduction of the Mona Lisa. The image is obtained form dark-field microscopy.*

1.  Rev. Mod. Phys., 88, 045006, (2016)
2. Small, 12, 446-451, (2016)
3. Nat Commun, 6, 7855, (2015).
4. Phys. Rev. X, 7, 011010, (2017).
5. Nature Communications, 10, 2442, (2019).
6. eLife, 7, e36608, (2018).